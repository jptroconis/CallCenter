package Model;

import Interfaces.IAtencion;

public class Director extends Empleado implements IAtencion {
	
	public Director(int id, IAtencion sucesor) {
		this.sucesor = sucesor;
		this.id = id;
	}
	
	public Director(int id) {
		this.id = id;
	};
    
}
