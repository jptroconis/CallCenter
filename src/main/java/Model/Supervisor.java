package Model;

import Interfaces.IAtencion;

public class Supervisor extends Empleado  {
	
	public Supervisor(int id, IAtencion sucesor) {
		this.sucesor = sucesor;
		this.id = id;
	}	
    
	public Supervisor(int id) {
		this.id = id;
	};

}
