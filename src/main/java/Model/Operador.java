package Model;

import Interfaces.IAtencion;

public class Operador extends Empleado {
	
    
	public Operador(int id, IAtencion sucesor) {
		this.sucesor = sucesor;
		this.id = id;
	}	
	
	public Operador(int id) {
		this.id = id;
	};

}
