package Model;

import Interfaces.IObservador;

public class Llamada implements Runnable {
	
	private long id;
	private final int M = 5;
	private final int N = 10;
	private final int MIL = 1000;
	private volatile boolean terminada = false;
	private Empleado empleado;
	private IObservador observador;
	
	public Llamada(long id) {
		this.id = id;
	}
	
	/**
	 * Logica de la llamada 
	 * @return
	 * @throws InterruptedException
	 */
	private void contestar() throws InterruptedException {
		int duracion = duracionLlamada();
		Thread.sleep(duracion*MIL);
		System.out.println(this.getClass().getName()+" "+getId()+" Terminada "+
					empleado.getClass().getName()+" "+empleado.getId()+" Disponible, "
							+ "Duracion "+duracion+" Segundos");
		terminada = true;
		observador.notificar();
	}
	
	/**
	 * Genera de manera aleatoria un numero entre 5 y 10 
	 * @return
	 */
	private synchronized int duracionLlamada() {
		return (int) Math.floor(Math.random()*(N-M+1)+M);
	}

	public void run() {
		try {
			contestar();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public synchronized boolean isTerminada() {
		return terminada;
	}
	
	public synchronized long getId() {
		return id;
	}
	
	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}
	
	public void agregarOnservador(IObservador observador) {
		this.observador = observador;
	}

}
