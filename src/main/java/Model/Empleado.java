package Model;

import Interfaces.IAtencion;

public abstract class Empleado implements IAtencion {
	
	protected long id;
	protected Llamada llamada;
	protected IAtencion sucesor;
	
	/**
	 * Metodo que contesta la llamada del usuario, la inicia en un hilo
	 * @param llamada
	 */
	protected synchronized void contestar(Llamada llamada) {
		this.llamada = llamada;
		this.llamada.setEmpleado(this);
		Thread hilo = new Thread(this.llamada);
		hilo.start();
	}
	
	/*
	 * (non-Javadoc)
	 * @Mirar documentacion en Interfaces.IAtencion#atender(Model.Llamada)
	 */
	public boolean atender(Llamada llamada) {
		synchronized (llamada) {
			if(this.isDisponible()){
				contestar(llamada);
				logAtencion();
				return true;
	        }else {
	        		return sucesor != null ? sucesor.atender(llamada) : false ;
	        }
		}
	}
	
	protected synchronized boolean isDisponible() {
		return this.llamada == null || this.llamada.isTerminada();
	}
	
	protected synchronized void logAtencion() {
		System.out.println("llamada "+this.llamada.getId()+" atendida por "+this.getClass().getName()+" "+getId());
	}
	
	public synchronized long getId() {
		return id;
	}

}
