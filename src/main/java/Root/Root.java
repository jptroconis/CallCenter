package Root;

import java.util.Scanner;

import Model.Llamada;
import Services.Dispatcher;

public class Root {

	private static Scanner lector;

	public static void main(String[] args) {
		
		lector = new Scanner(System.in);
		String entrada = null;
		
		long idLlamada = 0;
		Dispatcher dispatcher = new Dispatcher();
		long nLlamadas = -1;
		
		do {
		
			System.out.println("******************************************************");
			System.out.println("* PRESIONE ENTER PARA ATENDER UNA LLAMADA            *");
			System.out.println("* O DIGITE EL NUMERO DE LLAMADAS QUE DESEA ATENDER   *");
			System.out.println("* ESCRIBA LA TECLA 0 PARA NO RECIBIR MAS LLAMADADAS  *");
			System.out.println("* EL PROGRAMA SE CERRARA CUANDO SE HALLAN ATENDIDO   *");
			System.out.println("* LAS LLAMADAS PENDIENTES                            *");
			System.out.println("******************************************************");
			entrada = lector.nextLine();
			if(entrada.isEmpty()) {
				validarEspera(dispatcher.dispatchCall(new Llamada(++idLlamada)), idLlamada); 
			}else if(isNumeric(entrada)) {
				nLlamadas = Long.parseLong(entrada);
				for(int i=0; i<nLlamadas; i++) {
					validarEspera(dispatcher.dispatchCall(new Llamada(++idLlamada)), idLlamada); 
				}
			}
		} while(nLlamadas != 0);
		
	}
	
	/**
	 * Indica la llamada que se encuentra en espera.
	 * @param atendio
	 * @param idLlamada
	 */
	private static void validarEspera(boolean atendio, long idLlamada) {
		if(!atendio) {
			System.out.println("Llamada "+idLlamada+" En espera");
		}
	}
	
	/**
	 * Valida que se esté igresando un numero entero.
	 * @param cadena
	 * @return
	 */
	private static boolean isNumeric(String cadena){
		try {
			Integer.parseInt(cadena);
			return true;
		} catch (NumberFormatException nfe){
			return false;
		}
	}

}
