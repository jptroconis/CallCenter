package Services;

import Interfaces.IAtencion;
import Model.Director;
import Model.Llamada;
import Model.Operador;
import Model.Supervisor;

public class Dispatcher {
	
	/**Recursos del Dispacher**/
	private IAtencion inicial = null;
	private ColaController colaController = null;
	
	/**
	 * En este constructor se crean los 10 recursos que atenderan las llamadas
	 * En este caso se han configurado de la siguiente forma:
	 * Directores 2
	 * Supervisores 3
	 * Operadores 5
	 */
	public Dispatcher() {
		cargarDirectores(2);
		cargarSupervisores(3);
		cargarOperadores(5);
		colaController = new ColaController(inicial);
	}
	
	
	
	/**
	 * Metodo que atiende las llamadas recibidas
	 * @param llamada
	 * @return
	 */
	public boolean  dispatchCall(Llamada llamada) {
		llamada.agregarOnservador(colaController);
		boolean antendioLlamada = inicial.atender(llamada);
		if(!antendioLlamada) {
			colaController.agregarLlamadaEnEspera(llamada);
		}
		return antendioLlamada;
	}
	
	
	/**
	 * Carga los directores 
	 * @param cantidad
	 */
	private void cargarDirectores(int cantidad) {
		boolean existeInicial = inicial != null;
		int j = existeInicial ? cantidad : cantidad-1;
		inicial = !existeInicial ? new Director(cantidad) : inicial;
		for(int i=j; i>0;) {
			inicial = new Director(i--, inicial);
		}
	}
	
	/**
	 * Carga los supervisores
	 * @param cantidad
	 */
	private void cargarSupervisores(int cantidad) {
		boolean existeInicial = inicial != null;
		int j = existeInicial ? cantidad : cantidad-1;
		inicial = !existeInicial ? new Supervisor(cantidad) : inicial;
		for(int i=j; i>0;) {
			inicial = new Supervisor(i--, inicial);
		}
	}
	
	/**
	 * Carga los operadores
	 * @param cantidad
	 */
	private void cargarOperadores(int cantidad) {
		boolean existeInicial = inicial != null;
		int j = existeInicial ? cantidad : cantidad-1;
		inicial = !existeInicial ? new Operador(cantidad) : inicial;
		for(int i=j; i>0;) {
			inicial = new Operador(i--, inicial);
		}
	}
	


}
