package Services;

import java.util.ArrayDeque;
import java.util.Queue;

import Interfaces.IAtencion;
import Interfaces.IObservador;
import Model.Llamada;

public class ColaController implements IObservador {
	
	private volatile Queue<Llamada> llamadasEnEspera;
	private IAtencion inicial;
	private final int ESPERAR = 100;
	
	public ColaController(IAtencion inicial) {
		this.llamadasEnEspera = new ArrayDeque<Llamada>();;
		this.inicial = inicial;
	}
	
	/**
	 * Permite agregar una llamada a la cola de espera
	 * @param llamada
	 */
	public void agregarLlamadaEnEspera(Llamada llamada) {
		llamadasEnEspera.add(llamada);
	}
	
	/**
	 * Logica de procesamiento de la cola de llamadas en espera
	 * @throws InterruptedException
	 */
	private void procesarCola() throws InterruptedException {
		if(!llamadasEnEspera.isEmpty()) {
			Thread.sleep(ESPERAR);
			inicial.atender(llamadasEnEspera.poll());
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @Mirar documentacion en Interfaces.IObservador#notificar()
	 */
	public void notificar() {
		synchronized (this) {
			try {
				procesarCola();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}

}
