package Interfaces;

import Model.Llamada;

public interface IAtencion {
    
	/**
	 * Metodo de la interface que refleja el comportamiento de los empleados
	 * @param llamada
	 * @return
	 */
    public boolean atender(Llamada llamada);
   
}