package Interfaces;

public interface IObservador {
	
	/**
	 * Metodo que le permite notificar al observaodr que la tarea a terminado
	 */
	public void notificar();

}
