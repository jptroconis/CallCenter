import static org.junit.Assert.*;

import org.junit.Test;

import Model.Llamada;
import Services.Dispatcher;

public class DispatcherTest {

	@Test
	public void test10Llamadas() {
		assertTrue(test(10));
	}
	
	@Test
	public void testMasDe10Llamadas() {
		assertFalse(test(15));
	}
	
	private boolean test(int nllamadas) {
		Dispatcher dispatcher = new Dispatcher();
		int idLlamada = 0;
		boolean resultado = false;
		for(int i=0; i<nllamadas; i++) {
			resultado = dispatcher.dispatchCall(new Llamada(++idLlamada));
		}
		return resultado;
	}

}
